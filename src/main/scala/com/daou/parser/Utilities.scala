package com.daou.parser

import java.io.File
import java.text.SimpleDateFormat
import java.util.{Date, Locale}

object Utilities {
  def stringToDate(s: String, pattern: String): Date = {
    val formatter = new SimpleDateFormat(pattern, Locale.US)
    formatter.parse(s)
  }

  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList
    } else {
      List[File]()
    }
  }
}
