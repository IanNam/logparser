package com.daou.parser

class ApacheLogParser {
  private val ddd = "\\d{1,3}"
  private val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
  private val nioExe = "(\\S+)"
  private val client = "(\\S+)"
  private val user = "(\\S+)"
  private val dateTime = "(\\[.+?\\])"
  private val requestType = "(\\S+)"
  private val requestAPI = "(\\S+)"
  private val httpVersion = "(\\S+)"
  private val status = "(\\d{3})"
  private val bytes = "(\\S+)"
  private val referer = "(.*?)"
  private val browser = "(\\S+)"
  private val osType = "(\\(.+?\\))"
  private val agent = "(.*?)"
  private val device = "\"(.*?)\""
  private val duration = "(\\d.?\\d*)$"

  private val regex = s"$ip $nioExe $client $user $dateTime $requestType $requestAPI $httpVersion $status $bytes $referer $browser $osType $agent $device $duration".r

  def parseRecord(record: String): Option[ApacheLogRecord] = {
    record match {
      case regex(ip, nioExe, client, user, dateTime, requestType, requestAPI, httpVersion, status, bytes, referer, browser, osType, agent, device, duration) =>
        Some(ApacheLogRecord(ip, nioExe, client, user, dateTime, requestType, requestAPI, httpVersion, status, bytes, referer, browser, osType, agent, device, duration))
      case _ => None
    }
  }
}
