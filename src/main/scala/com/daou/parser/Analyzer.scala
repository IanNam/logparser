package com.daou.parser

import java.io.File
import java.io.PrintWriter
import java.util.Date
import scala.io.Source

import Utilities._

object Analyzer {
  val INPUT_DIR = "D:\\LogParser\\src\\main\\resource\\input"
  val OUTPUT_DIR = "D:\\LogParser\\src\\main\\resource\\output"
  val pw = new PrintWriter(s"$OUTPUT_DIR\\server.log")

  def main(args: Array[String]): Unit = {
    getListOfFiles(INPUT_DIR).par.map(analyze(_))
    pw.close()
  }

  def analyze(file: File): Unit = {
    val transformedLogData = preprocessor(Source.fromFile(file).getLines)

    LogTransformer.filterByTime(transformedLogData.toList,
      stringToDate("2018-08-28 00:00:00", "yyyy-MM-dd HH:mm:ss"),
      stringToDate("2018-08-28 00:00:03", "yyyy-MM-dd HH:mm:ss"))
  }

  def preprocessor(lines: Iterator[String]): Iterator[(Date, ApacheLogRecord)] = {
    val parser: ApacheLogParser = new ApacheLogParser
    lines.map(parser.parseRecord(_))
      .filter(_.isDefined)
      .map(_.get)
      .filterNot(_.requestAPI.toLowerCase.endsWith(".png"))
      .filterNot(_.requestAPI.toLowerCase.endsWith(".jpg"))
      .filterNot(_.requestAPI.toLowerCase.endsWith(".css"))
      .map(x => {
        val requestDate = x.dateTime.replace("[", "").replace("]", "")
        stringToDate(requestDate, "dd/MMM/yyyy:HH:mm:ss Z") -> x
      })
  }
}

