package com.daou.parser

import java.util.Date

object LogTransformer {
  // 특정시간의 응답시간 오래 걸린 log 추출
  def filterByDuration(log: List[(Date, ApacheLogRecord)], threshold: Double): List[(Date, ApacheLogRecord)] =
    log.filter(_._2.duration.toDouble > threshold)

  // 특정시간에 기록된 log 추출
  def filterByTime(log: List[(Date, ApacheLogRecord)], from: Date, to: Date): List[(Date, ApacheLogRecord)] =
    log.filter(_._1.before(to)).filter(_._1.after(from))
}
