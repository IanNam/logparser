package com.daou.parser

case class ApacheLogRecord(
                            ip: String,
                            nioExe: String,
                            client: String,
                            user: String,
                            dateTime: String,
                            requestType: String,
                            requestAPI: String,
                            httpVersion: String,
                            status: String,
                            bytes: String,
                            referer: String,
                            browser: String,
                            osType: String,
                            agent: String,
                            device: String,
                            duration: String
                          ) extends Serializable
